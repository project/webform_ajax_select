jQuery(function ($) {
    'use strict';

    var elementsCache = {};
    var htmlCache = {};
    var ajaxProcess = 0;
    var isNewRequest = 0;

    var loadSelectData = function () {
        // Do nothing if process already running.
        if (ajaxProcess) {
            return;
        }
        $('select.webform-select-ajax').each(function (i, el) {
            var $el = $(el);
            var key = $el.data('key');
            var isNewRequest = !elementsCache[key];
            // Register element for ajax callback.
            if (isNewRequest) {
                elementsCache[key] = [];
            }
            elementsCache[key].push($el);
            // Only one request per form-select key.
            // TODO: make it parallell.
            if (isNewRequest) {
                var url = Drupal.settings.webform_select_ajax.resource_path + '/' + key + '.html';
                $.ajax({url: url, success: function (html) {
                    // Iterate registered elements for callback.
                    htmlCache[key] = html;
                    for (var i = 0; i < elementsCache[key].length; i++) {
                        elementsCache[key][i].html(html);
                    }
                }, complete: function () {
                    ajaxProcess = 0;
                }});
            }
            // Load options from cache if already loaded.
            if (elementsCache[key] && !ajaxProcess) {
                // Iterate registered elements for callback.
                for (var k = 0; k < elementsCache[key].length; k++) {
                    elementsCache[key][k]
                        .html(htmlCache[key])
                        .removeClass('webform-select-ajax');
                }
            }
        });
        // If request is new, then there were ajax.
        if (isNewRequest) {
            ajaxProcess = 1;
        }
    };
    $(document).ajaxStop(loadSelectData);
    loadSelectData();
});
