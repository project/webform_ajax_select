Description
-----------
This module adds ability for webform to load select element with ajax and then
render it to your form.

Requirements
------------
Drupal 7.x
Webform 4.x
PHP 5.3

Installation
------------
1. Copy the entire webform directory the Drupal sites/all/modules directory.
2. Login as an administrator. Enable the module in the "Administer" -> "Modules"
3. Edit any created webform select element and check `Load with ajax` option.
